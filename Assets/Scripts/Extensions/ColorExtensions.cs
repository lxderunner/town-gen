using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Extensions
{
    public static class ColorExtensions
    {
        public static Color GetNiceHSVColor(this Color color) => Color.HSVToRGB(Random.Range(0f, 1f), 1, 1);
    }
}