using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace nftGen.Extensions
{
    public static class GameObjectExtensions
    {
        public static Bounds GetNestedBoundsSpriteRenderer(this GameObject gameObject)
        {
            var spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>().ToList();
            if(spriteRenderers.Count == 0)
            {
                Debug.Log("No Sprite Renderers attached!");
                return new Bounds();
            }
            var bounds = spriteRenderers.FirstOrDefault().bounds;
            spriteRenderers.Where(sr => sr != spriteRenderers.FirstOrDefault()).ToList().ForEach(sr => bounds.Encapsulate(sr.bounds));
            return bounds;
        }

        public static Bounds GetListBoundsSpriteRenderer(this List<GameObject> gameObjects)
        {
            var spriteRenderersInSpriteRenderers = gameObjects.Select(g => g.GetComponentsInChildren<SpriteRenderer>()).ToList();
            var spriteRenderers = spriteRenderersInSpriteRenderers.SelectMany(x => x).ToList();
            if (spriteRenderers.Count == 0)
            {
                Debug.Log("No Sprite Renderers attached!");
                return new Bounds();
            }
            var bounds = spriteRenderers.FirstOrDefault().bounds;
            spriteRenderers.Where(sr => sr != spriteRenderers.FirstOrDefault()).ToList().ForEach(sr => bounds.Encapsulate(sr.bounds));
            return bounds;
        }

        public static Bounds GetNestedBoundsMeshRenderer(this GameObject gameObject)
        {
            var meshRenderers = gameObject.GetComponentsInChildren<MeshRenderer>().ToList();
            if (meshRenderers.Count == 0)
            {
                Debug.Log("No Mesh Renderers attached!");
                return new Bounds();
            }
            var bounds = meshRenderers.FirstOrDefault().bounds;
            meshRenderers.Where(sr => sr != meshRenderers.FirstOrDefault()).ToList().ForEach(sr => bounds.Encapsulate(sr.bounds));
            return bounds;
        }

        public static Bounds GetListBoundsMeshRenderer(this List<GameObject> gameObjects)
        {
            var meshRenderersInMeshRenderers = gameObjects.Select(g => g.GetComponentsInChildren<MeshRenderer>()).ToList();
            var meshRenderers = meshRenderersInMeshRenderers.SelectMany(x => x).ToList();
            if (meshRenderers.Count == 0)
            {
                Debug.Log("No Mesh Renderers attached!");
                return new Bounds();
            }
            var bounds = meshRenderers.FirstOrDefault().bounds;
            meshRenderers.Where(sr => sr != meshRenderers.FirstOrDefault()).ToList().ForEach(sr => bounds.Encapsulate(sr.bounds));
            return bounds;
        }
    }
}