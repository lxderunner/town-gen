using nftGen.Behaviour.Street;
using nftGen.Component.Street;
using nftGen.Component.Town;
using nftGen.Extensions;
using nftGen.SO.Town;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace nftGen.Behaviour.Town
{
    public class TowerGenerationBehaviourByConfig : ITownGenerationBehaviour
    {
        [OdinSerialize] private TownGenerationSettings townGenerationSettings;
        public BaseTown Generate()
        {
            var townGameObject = new GameObject("Town");
            var town = townGameObject.AddComponent<BaseTown>();

            var streetCount = Random.Range(townGenerationSettings.streetsCountMinMax.x, townGenerationSettings.streetsCountMinMax.y);
            var streetGenerationBehaviour = townGenerationSettings.streetGenerationBehaviours[Random.Range(0, townGenerationSettings.streetGenerationBehaviours.Count)];
            var street = GenerateBaseStreet(Vector3.zero, streetGenerationBehaviour);
            town.BaseStreets.Add(street);

            for (int i = 0; i < streetCount; i++)
            {
                streetGenerationBehaviour = townGenerationSettings.streetGenerationBehaviours[Random.Range(0, townGenerationSettings.streetGenerationBehaviours.Count)];
                var lastStreet = town.BaseStreets.LastOrDefault();
                var newStreetPosition = new Vector3(0, 0, -Random.Range(townGenerationSettings.betweenStreetsSpaceMinMax.x, townGenerationSettings.betweenStreetsSpaceMinMax.y) + lastStreet.gameObject.transform.position.z);
                street = GenerateBaseStreet(newStreetPosition, streetGenerationBehaviour);
                town.BaseStreets.Add(street);
            }

            townGameObject.transform.position = town.BaseStreets.Select(bs => bs.gameObject).ToList().GetListBoundsMeshRenderer().center;
            town.BaseStreets.Select(bs => bs.transform).ToList().ForEach(bst => bst.parent = townGameObject.transform);

            return town;
        }

        public BaseStreet GenerateBaseStreet(Vector3 position, IStreetGenerationBehaviour streetGenerationBehaviour)
        {
            return streetGenerationBehaviour.Generate(position);
        }
    }
}