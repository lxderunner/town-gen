using nftGen.Component.Town;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Behaviour.Town
{
    public interface ITownGenerationBehaviour
    {
        public BaseTown Generate();
    }
}