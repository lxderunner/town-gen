using nftGen.Behaviour.Building;
using nftGen.Component.Building;
using nftGen.Component.Street;
using nftGen.Extensions;
using nftGen.SO.Street;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace nftGen.Behaviour.Street
{
    public class StreetGenerationBehaviourByConfig : IStreetGenerationBehaviour
    {
        [OdinSerialize] private StreetGenerationSettings streetGenerationSettings;
        public BaseStreet Generate(Vector3 position)
        {
            var streetGameObject = new GameObject("Street");
            var street = streetGameObject.AddComponent<BaseStreet>();

            var buildingsCount = Random.Range(streetGenerationSettings.buildingsCountMinMax.x, streetGenerationSettings.buildingsCountMinMax.y);
            var buildingGenerationBehaviour = streetGenerationSettings.buildingGenerationBehaviours[Random.Range(0, streetGenerationSettings.buildingGenerationBehaviours.Count)];
            var building = GenerateBaseBuilding(Vector3.zero, buildingGenerationBehaviour);
            street.BaseBuildings.Add(building);

            for(int i = 0; i < buildingsCount; i++)
            {
                buildingGenerationBehaviour = streetGenerationSettings.buildingGenerationBehaviours[Random.Range(0, streetGenerationSettings.buildingGenerationBehaviours.Count)];
                var betweenBuildingSpace = Random.Range(streetGenerationSettings.betweenBuildingsSpaceMinMax.x, streetGenerationSettings.betweenBuildingsSpaceMinMax.y);
                var lastBuilding = street.BaseBuildings.LastOrDefault().gameObject;
                var newBuildingPosition = new Vector3(lastBuilding.transform.position.x + lastBuilding.GetNestedBoundsMeshRenderer().size.x * 0.5f + betweenBuildingSpace, 0, 0);
                building = GenerateBaseBuilding(newBuildingPosition, buildingGenerationBehaviour);
                street.BaseBuildings.Add(building);
            }

            streetGameObject.transform.position = street.BaseBuildings.Select(bb => bb.gameObject).ToList().GetListBoundsMeshRenderer().center;
            street.BaseBuildings.Select(bb => bb.transform).ToList().ForEach(bbt => bbt.parent = streetGameObject.transform);
            streetGameObject.transform.position += position;
            return street;
        }

        public BaseBuilding GenerateBaseBuilding(Vector3 position, IBuildingGenerationBehaviour buildingGenerationBehaviour)
        {
            return buildingGenerationBehaviour.Generate(position);
        }
    }
}