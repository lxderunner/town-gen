using nftGen.Component.Street;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Behaviour.Street
{
    public interface IStreetGenerationBehaviour
    {
        public BaseStreet Generate(Vector3 position);
    }
}