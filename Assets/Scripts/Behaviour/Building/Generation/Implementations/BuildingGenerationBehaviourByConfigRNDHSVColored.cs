using nftGen.Component.Building;
using nftGen.Component.Street;
using nftGen.Component.Wall;
using nftGen.Extensions;
using nftGen.SO.Building;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace nftGen.Behaviour.Building
{
    public class BuildingGenerationBehaviourByConfigRNDHSVColored : IBuildingGenerationBehaviour
    {
        [OdinSerialize] private BuildingGenerationSettings buildingGenerationSettings;
        [OdinSerialize] [Range(0, 1)] private float colorSaturation;
        public BaseBuilding Generate(Vector3 position)
        {
            var buildingGameObject = new GameObject("Building");
            var building = buildingGameObject.AddComponent<BaseBuilding>();
            var basePosition = new Vector3(position.x + buildingGenerationSettings.wallPrefab.GetNestedBoundsMeshRenderer().size.x * 0.5f, 0, 0);
            var sizeX = Random.Range(buildingGenerationSettings.sizeXMinMax.x, buildingGenerationSettings.sizeXMinMax.y);
            var sizeY = Random.Range(buildingGenerationSettings.sizeYMinMax.x, buildingGenerationSettings.sizeYMinMax.y);
            var color = Color.HSVToRGB(Random.Range(0f, 1f), colorSaturation, 1);
            for(int x = 0; x < sizeX; x++)
            {
                for(int y = 0; y < sizeY; y++)
                {
                    var wallGameObject = GameObject.Instantiate(buildingGenerationSettings.wallPrefab);
                    var wall = wallGameObject.GetComponent<BaseWall>();
                    var meshRenderer = wall.GetComponentInChildren<MeshRenderer>();
                    wallGameObject.transform.position = basePosition + new Vector3(x * wallGameObject.GetNestedBoundsMeshRenderer().size.x, y * wallGameObject.GetNestedBoundsMeshRenderer().size.y, 0);
                    meshRenderer.material.color = color;
                    building.BaseWalls.Add(wall);
                }
            }

            buildingGameObject.transform.position = building.BaseWalls.Select(bw => bw.gameObject).ToList().GetListBoundsMeshRenderer().center;
            building.BaseWalls.Select(bw => bw.transform).ToList().ForEach(bwt => bwt.parent = buildingGameObject.transform);

            return building;
        }
    }
}
