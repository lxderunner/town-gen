using nftGen.Component.Building;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Behaviour.Building
{
    public interface IBuildingGenerationBehaviour
    {
        public BaseBuilding Generate(Vector3 position);

    }
}