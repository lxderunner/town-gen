using nftGen.Behaviour.Building;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.SO.Street
{
    [CreateAssetMenu(fileName = "StreetGenerationSettings", menuName = "SO/nftGen/Street/Generation/StreetGenerationSettings")]
    public class StreetGenerationSettings : SerializedScriptableObject
    {
        public Vector2Int buildingsCountMinMax;
        public Vector2 betweenBuildingsSpaceMinMax;
        public List<IBuildingGenerationBehaviour> buildingGenerationBehaviours;
    }
}