using nftGen.Behaviour.Street;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.SO.Town
{
    [CreateAssetMenu(fileName = "TownGenerationSettings", menuName = "SO/nftGen/Town/Generation/TownGenerationSettings")]
    public class TownGenerationSettings : SerializedScriptableObject
    {
        public Vector2Int streetsCountMinMax;
        public Vector2 betweenStreetsSpaceMinMax;
        public List<IStreetGenerationBehaviour> streetGenerationBehaviours;
    }
}