using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.SO.Building
{
    [CreateAssetMenu(fileName = "BuildingGenerationSettings", menuName = "SO/nftGen/Building/Generation/BuildingGenerationSettings")]
    public class BuildingGenerationSettings : SerializedScriptableObject
    {
        public GameObject wallPrefab;
        public Vector2Int sizeXMinMax;
        public Vector2Int sizeYMinMax;
    }
}