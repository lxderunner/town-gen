using nftGen.Component.Building;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Component.Street
{
    public class BaseStreet : SerializedMonoBehaviour
    {
        [OdinSerialize] private List<BaseBuilding> baseBuildings;

        public List<BaseBuilding> BaseBuildings { get => baseBuildings; set => baseBuildings = value; }

        private void Awake()
        {
            baseBuildings = new List<BaseBuilding>();
        }
    }
}