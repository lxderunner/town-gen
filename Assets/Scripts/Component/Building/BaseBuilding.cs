using nftGen.Component.Wall;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Component.Building
{
    public class BaseBuilding : SerializedMonoBehaviour
    {
        [OdinSerialize] private List<BaseWall> baseWalls;

        public List<BaseWall> BaseWalls { get => baseWalls; set => baseWalls = value; }

        private void Awake()
        {
            baseWalls = new List<BaseWall>();
        }
    }
}