using nftGen.Behaviour.Street;
using nftGen.Behaviour.Town;
using nftGen.Component.Street;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Component.Town
{
    public class TownGenerator : SerializedMonoBehaviour
    {
        [OdinSerialize] private BaseTown baseTown;
        [OdinSerialize] private ITownGenerationBehaviour townGenerationBehaviour;
        private void Start()
        {
            baseTown = townGenerationBehaviour.Generate();
        }
    }
}