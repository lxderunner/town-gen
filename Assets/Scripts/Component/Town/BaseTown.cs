using nftGen.Component.Street;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace nftGen.Component.Town
{
    public class BaseTown : SerializedMonoBehaviour
    {
        [OdinSerialize] private List<BaseStreet> baseStreets;

        public List<BaseStreet> BaseStreets { get => baseStreets; set => baseStreets = value; }

        private void Awake()
        {
            baseStreets = new List<BaseStreet>();
        }
    }
}